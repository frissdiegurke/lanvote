module.exports = (grunt) ->

  jsLibs =
    common: [
      'lib/jquery/dist/jquery.js'
      'lib/spinjs/spin.js'
      'lib/socket.io-client/socket.io.js'
      'lib/angular/angular.js'
      'lib/angular-route/angular-route.js'
      'lib/angular-animate/angular-animate.js'
      'lib/angular-sanitize/angular-sanitize.js'
    ]
    open: []
    closed: [
      'lib/jquery-ui/ui/widget.js'
      'lib/jquery-ui/ui/widgets/mouse.js'
      'lib/jquery-ui/ui/widgets/slider.js'
      'lib/jquery-ui/ui/widgets/progressbar.js'
      'lib/jquery-ui/ui/widgets/menu.js'
      'lib/jquery-ui/ui/widgets/autocomplete.js'
      'lib/jquery-ui/ui/widgets/datepicker.js'
      'lib/jquery-ui/ui/position.js'
      'lib/jquery-ui/ui/i18n/datepicker-de.js'
      'lib/jqueryui-touch-punch/jquery.ui.touch-punch.js'
    ]
    admin: []

  minLibs =
    'lib/jquery/dist/jquery.js': 'lib/jquery/dist/jquery.min.js'
    'lib/angular/angular.js': 'lib/angular/angular.min.js'
    'lib/angular-route/angular-route.js': 'lib/angular-route/angular-route.min.js'
    'lib/angular-animate/angular-animate.js': 'lib/angular-animate/angular-animate.min.js'
    'lib/angular-sanitize/angular-sanitize.js': 'lib/angular-sanitize/angular-sanitize.min.js'
    'lib/jquery-ui/ui/widget.js': 'lib/jquery-ui/ui/minified/widget.js'
    'lib/jquery-ui/ui/position.js': 'lib/jquery-ui/ui/minified/position.js'
    'lib/jquery-ui/ui/i18n/datepicker-de.js': 'lib/jquery-ui/ui/minified/i18n/datepicker-de.js'
    'lib/jqueryui-touch-punch/jquery.ui.touch-punch.js': 'lib/jqueryui-touch-punch/jquery.ui.touch-punch.min.js'

  distCopySources = [
    'img/*',
    'views/**/*',
    'templates/**/*',
    'i18n/*.json',
    'favicon.ico'
  ]
  devCopySources = distCopySources.concat [
    'index-dev.html'
    'lib/**/*'
    'backend-mock/**/*.json'
  ]

  mods =
    common: ['i18n', 'util', 'common']
    open: ['open']
    closed: ['closed']
    admin: ['admin']

  jsSrcPath = '.tmp/scripts'
  getNGMods = (mods) ->
    Array.prototype.concat.apply([], mods.map (mod) -> [
        "#{jsSrcPath}/#{mod}/main.js"
        "#{jsSrcPath}/#{mod}/**/*.js"
      ]
    )
  getModLibs = (mod) -> jsLibs[mod].map (dep) -> ".tmp/#{dep}"
  getModLibsMin = (mod) ->
    jsMinLibs[mod].map (dep) -> ".tmp/#{dep}"
  getJs = (mod) -> getModLibs(mod).concat getNGMods mods[mod]

  jsMinLibs = {}
  for mod of jsLibs
    jsMinLibs[mod] = jsLibs[mod].map (lib) -> minLibs[lib] || lib

  jsCommon = getJs 'common'
  jsOpen = getJs 'open'
  jsClosed = getJs 'closed'
  jsAdmin = getJs 'admin'

  lessSources = 'styles/**/*.less'
  coffeeSources = 'scripts/**/*.coffee'

  # helpers
  htmlbuildDev = (app, scripts) ->
    src: 'app/index.html'
    dest: ".tmp/#{app}.html"
    options:
      relative: true
      scripts:
        main: scripts
      styles:
        main: '.tmp/styles/main.css'
      sections:
        nav: "app/static-templates/nav-#{app}.html"
        footer: "app/static-templates/footer-#{app}.html"
      data:
        app: app
        manageOrAdd: if app == 'closed' then 'add' else 'manage'

  htmlbuildDist = (app, scripts) ->
    src: 'app/index.html'
    dest: "dist/#{app}.html"
    options:
      relative: true
      scripts:
        main: scripts.map (scr) -> "dist/scripts/#{scr}.min.js"
      styles:
        main: 'dist/styles/main.min.css'
      sections:
        nav: "app/static-templates/nav-#{app}.html"
        footer: "app/static-templates/footer-#{app}.html"
      data:
        app: app
        manageOrAdd: if app == 'closed' then 'add' else 'manage'

  config =
    pkg: grunt.file.readJSON 'package.json'

    bower:
      install: {}

    less:
      dev:
        options:
          sourceMap: true
          sourceMapRootpath: '../'
          sourceMapBasepath: '.tmp/'
        files:
          ".tmp/styles/main.css": ".tmp/styles/main.less"
      dist:
        options:
          compress: true
        files:
          "dist/styles/main.min.css": "app/styles/main.less"

    coffee:
      all:
        options:
          sourceMap: true
        expand: true
        cwd: '.tmp/scripts'
        src: '**/*.coffee'
        dest: '.tmp/scripts'
        ext: '.js'

    karma:
      options:
        configFile: 'karma.conf.js'
      dev: {}
      dist:
        singleRun: true
        autoWatch: false
        browsers: ['PhantomJS']
      ci:
        singleRun: true
        autoWatch: false
        reporters: ['junit']
        junitReporter:
          outputFile: 'test-results.xml'
        browsers: ['PhantomJS']

    uglify:
      options:
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %> */'
      dist:
        files:
          'dist/scripts/common.min.js': getNGMods mods.common
          'dist/scripts/open.min.js': getNGMods mods.open
          'dist/scripts/closed.min.js': getNGMods mods.closed
          'dist/scripts/admin.min.js': getNGMods mods.admin

    concat:
      options:
        separator: ';\n'
      dist:
        files:
          'dist/scripts/common.min.js': getModLibsMin('common').concat(['dist/scripts/common.min.js'])
          'dist/scripts/open.min.js': getModLibsMin('open').concat(['dist/scripts/open.min.js'])
          'dist/scripts/closed.min.js': getModLibsMin('closed').concat(['dist/scripts/closed.min.js'])
          'dist/scripts/admin.min.js': getModLibsMin('admin').concat(['dist/scripts/admin.min.js'])

    watch:
      grunt:
        files: 'Gruntfile.coffee'
        tasks: ['dist', 'dev_build']
      less:
        files: "app/#{lessSources}"
        tasks: ['clean:css', 'copy:dev_less', 'less:dev', 'less:dist']
      coffee:
        files: "app/#{coffeeSources}"
        tasks: ['clean:js', 'copy:dev_coffee', 'coffee', 'uglify', 'concat', 'htmlbuild_dev', 'htmlbuild_dist']
      copy:
        files: (devCopySources.map (src) -> "app/#{src}").concat distCopySources.map (src) -> "app/#{src}"
        tasks: ['clean:copy', 'copy:dev', 'copy:dist']
      htmlbuild:
        files: ['app/index.html', 'app/static-templates/*.html']
        tasks: ['clean:html', 'htmlbuild_dev', 'htmlbuild_dist']

    htmlbuild:
      dev_open: htmlbuildDev 'open', jsCommon.concat jsOpen
      dev_closed: htmlbuildDev 'closed', jsCommon.concat jsClosed
      dev_admin: htmlbuildDev 'admin', jsCommon.concat(jsClosed, jsAdmin)
      dist_open: htmlbuildDist('open', ['common', 'open'])
      dist_closed: htmlbuildDist('closed', ['common', 'closed'])
      dist_admin: htmlbuildDist('admin', ['common', 'closed', 'admin'])

    copy:
      dev:
        expand: true
        cwd: 'app'
        src: devCopySources
        dest: '.tmp'
        rename: (dest, src) ->
          "#{dest}/#{src.replace(/(backend-mock\/|\.json$|-dev)/g, '')}"
      dev_less:
        expand: true
        cwd: 'app'
        src: lessSources
        dest: '.tmp'
      dev_coffee:
        expand: true
        cwd: 'app'
        src: coffeeSources
        dest: '.tmp'
      dist:
        expand: true
        cwd: 'app'
        src: distCopySources
        dest: 'dist'
        rename: (dest, src) ->
          "#{dest}/#{src.replace(/\.json$/g, '')}"

    clean:
      all: ["dist", ".tmp", '!dist/game-art']
      html: ['dist/*.html', '.tmp/*.html']
      css: ['dist/styles', '.tmp/styles']
      js: ['dist/scripts', '.tmp/scripts']
      copy: (distCopySources.map (src) -> "dist/#{src}").concat devCopySources.map (src) -> ".tmp/#{src}"

    symlink:
      dev:
        src: 'dist/game-art'
        dest: '.tmp/game-art'

  grunt.initConfig config

  grunt.loadNpmTasks 'grunt-bower-task'
  grunt.loadNpmTasks 'grunt-contrib-less'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-karma'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-concat'
  grunt.loadNpmTasks 'grunt-html-build'
  grunt.loadNpmTasks 'grunt-contrib-copy'
  grunt.loadNpmTasks 'grunt-contrib-clean'
  grunt.loadNpmTasks 'grunt-contrib-symlink'

  grunt.registerTask 'htmlbuild_dev', ['htmlbuild:dev_open', 'htmlbuild:dev_closed', 'htmlbuild:dev_admin']
  grunt.registerTask 'htmlbuild_dist', ['htmlbuild:dist_open', 'htmlbuild:dist_closed', 'htmlbuild:dist_admin']
  grunt.registerTask 'copy_dev', ['copy:dev', 'copy:dev_less', 'copy:dev_coffee']
  grunt.registerTask 'compile_dev', ['coffee', 'less:dev', 'htmlbuild_dev']
  grunt.registerTask 'compile_dist', ['coffee', 'less:dist', 'uglify', 'concat', 'htmlbuild_dist']
  grunt.registerTask 'dev_build', ['bower', 'copy_dev', 'compile_dev', 'symlink:dev']
  grunt.registerTask 'dev', ['dev_build', 'watch']
  grunt.registerTask 'test', ['bower', 'karma:dev']
  grunt.registerTask 'dist', ['bower', 'copy', 'compile_dist']
  grunt.registerTask 'ddev', ['dist', 'watch']
  grunt.registerTask 'ci', ['dist','karma:ci']