mod = angular.module 'admin'

mod.controller 'manageGamesCtrl', ['$scope', 'games', 'manageGames', ($scope, games, manageGames) ->
  $scope.newGame =
    votingType: 1
  $scope.createNewGame = ->
    manageGames.create $scope.newGame
    $scope.addNew = false;
    $scope.newGame =
      votingType: 1
  $scope.games = games.all
  $scope.preview =
    text: ''
    showImg: (img) ->
      $scope.preview.url = img
      $scope.preview.show = true
]