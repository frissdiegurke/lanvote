mod = angular.module 'admin'

mod.controller 'manageUsersCtrl', ['$scope', 'manageUsers', ($scope, manageUsers) ->
  $scope.users = manageUsers
]