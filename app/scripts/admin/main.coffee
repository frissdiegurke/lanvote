app = angular.module 'admin', ['ngRoute', 'closed']

app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider
  .when '/manage-games',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
        gamesLoad: ['games', 'result', (games) -> games.load()]
      templateUrl: 'views/manage-games-admin.html'
      controller: 'manageGamesCtrl'
  .when '/manage-users',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
        statusLoad: ['user', (user) -> user.load()]
        votingLoad: ['voting', (voting) -> voting.load()]
        manageUsersLoad: ['manageUsers', 'result', (manageUsers) -> manageUsers.load()]
      templateUrl: 'views/manage-users.html'
      controller: 'manageUsersCtrl'
  .when '/voting',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
        statusLoad: ['user', (user) -> user.load()]
        gamesLoad: ['games', (games) -> games.load()]
        votingLoad: ['voting', 'result', (voting) -> voting.load()]
      templateUrl: 'views/voting.html'
      controller: 'votingCtrl'
]