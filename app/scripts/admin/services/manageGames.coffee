mod = angular.module 'admin'

mod.factory 'manageGames', ['socket', 'toaster', '$timeout', (socket, toaster) ->
  service = {}
  socket.on 'game.activate', (title) ->
    toaster.info 'game.activate', [title]
  service.delete = (game) ->
    socket.query 'games.delete', id: game.id
  service.create = (game) ->
    socket.query 'games.create', game
  service.update = (game) ->
    socket.query 'games.update', game
  service
]