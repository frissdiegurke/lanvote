mod = angular.module 'common'

mod.factory 'chat', ['socket', '$timeout', (socket, $timeout) ->
  service =
    peers: []
    selectedPeers: []
    history: []
    message: ''
  username = null

  socket.query 'self.username', (answer) ->
    username = answer
    refreshPeerList service.peers

  formatDate = (date) ->
    hours = date.getHours()
    minutes = date.getMinutes()
    hours = '0' + hours if hours < 10
    minutes = '0' + minutes if minutes < 10
    "#{hours}:#{minutes} " + if hours < 12 then 'AM' else 'PM'

  # FIXME: Very hacky ... Use repeating view instead and simply push messages to array.
  appendEntry = (entry, sent = false) ->
    entry.classes = {} if !entry.classes?
    entry.classes.sent = sent
    entry.classes.private = !sent && entry.recipients.length == 1
    entry.classes.group = !sent && entry.recipients.length < service.peers.length
    entry.time = formatDate new Date(entry.time)
    $timeout ->
      area = $('.chatArea > messages')
      list = $('ol', area)
      scroll = (list.height() - area.height()) - area.scrollTop() < 3
      service.history.push entry
      $timeout( -> area.scrollTop list.height(), 10) if scroll
#TODO maybe there is a better way than a timeout to be sure that entry has been added by angular

  socket.on 'chat.message', (entry) ->
    appendEntry entry if entry.sender.ip != username # TODO check for user-id

  socket.on 'chat.message.sent', (entry) ->
    appendEntry entry, true

  refreshPeerList = (peers) ->
    $timeout ->
      service.peers = peers.filter (peer) -> peer.ip != username

  socket.on 'chat.peers', refreshPeerList

  service.send = ->
    return null if service.message.trim() == ''
    recipients = service.selectedPeers
    if !recipients.length
      recipients = []
      recipients.push peer.ip for peer in service.peers
    entry = {time: Date.now(), message: service.message, recipients: recipients}
    socket.emit 'chat.message', entry
    $timeout ->
      service.message = ''

  service
]