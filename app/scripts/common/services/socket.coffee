mod = angular.module 'common'

mod.factory 'socket', ['$location', '$q', '$http', ($location, $q, $http) ->
  service = {}
  socket = null
  connected = null
  id = 0
  queryCallbacks = {}

  onConnectionEstablished = ->
    socket.on 'query', (response) ->
      console.error response.error if response.error
      queryCallbacks[response._id] response.data
      delete queryCallbacks[response._id]

  connected = $q.defer()
  socket = io.connect "#{$location.protocol()}://#{$location.host()}:#{$location.port()}"
  socket.on 'connection established', ->
    $http.get('/socketToken').then (res) ->
      socket.emit 'authorize', res.data.token
  socket.on 'authorized', ->
    connected.resolve 'ready'
    onConnectionEstablished()

  service.connect = ->
    connected.promise

  service.on = (messageId, callback) ->
    socket.on messageId, callback

  service.query = (method, data, callback) ->
    def = $q.defer()
    service.connect().then ->
      id++
      if angular.isFunction data # make data optional
        callback = data
        data = {}
      data._id = id
      data.method = method
      queryCallbacks[id] = (data)->
        callback data if callback?
        def.resolve data
      socket.emit 'query', data
    def.promise

  service.emit = (messageId, data) ->
    service.connect().then ->
      socket.emit messageId, data

  service
]