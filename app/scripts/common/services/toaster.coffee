mod = angular.module 'common'

mod.factory 'toaster', ['$timeout', ($timeout) ->
  service =
    toasts: []
    visibleToasts: []
    class: ''
  classes = ['', 'success', 'info', 'error']
  defTimes = [0, 3000, 2000, 5000]
  maxVisible = 5
  nextId = 0
  open = false
  setState = (num) ->
    service.class = classes[num]
  refreshState = ->
    maxLevel = 0
    for toast in service.toasts
      maxLevel = toast.level if toast.level > maxLevel
    setState maxLevel
  addToast = (toast) ->
    service.toasts.unshift toast
    service.visibleToasts.unshift toast if !open
    service.visibleToasts.pop() while service.visibleToasts.length > maxVisible and !open
  message = (msg, params, level, time) ->
    params = [params] if !(params instanceof Array)
    id = nextId
    nextId++
    $timeout -> # to apply to $rootScope
      addToast {
        id: id
        level: level
        class: classes[level]
        message: 'header.toasts.' + classes[level] + '.' + msg
        params: params
      }
      refreshState()
      $timeout(( ->
        service.hide(id) if !open
      ), time)
    id
  service.clear = ->
    service.toasts = []
    service.visibleToasts = []
    refreshState()
  service.remove = (msg, params) ->
    if typeof msg == 'number'
      service.hide(msg)
      service.toasts = service.toasts.filter (toast) ->
        toast.id != msg
      refreshState()
    else
      for toast in service.toasts
        service.remove toast.id if toast.message == msg and angular.equals(toast.params, params)
  service.hide = (id) ->
    service.visibleToasts = service.visibleToasts.filter (toast) ->
      toast.id != id
  service.toggle = ->
    open = if service.toasts.length then !open else false
    service.visibleToasts = if open then service.toasts else []
  service.success = (msg, params = [], time = defTimes[1]) ->
    message(msg, params, 1, time)
  service.info = (msg, params = [], time = defTimes[2]) ->
    message(msg, params, 2, time)
  service.error = (msg, params = [], time = defTimes[3]) ->
    message(msg, params, 3, time)
  service
]