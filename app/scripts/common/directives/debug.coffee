mod = angular.module 'common'

mod.directive 'debug', ['$location', ($location) ->
  scope: true
  transclude: true
  template: '<div ng-transclude ng-if="debug"></div>'
  restrict: 'EA'
  link: ($scope) ->
    $scope.debug = $location.search().debug?
]