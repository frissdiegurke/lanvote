mod = angular.module 'i18n'

mod.factory 'i18n', ['$http', '$locale', '$window', ($http, $locale, $window) ->
  mapping = null
  promise = null
  load = (locale) ->
    if promise?
      promise
    else
      stored = if $window.localStorage? then $window.localStorage.getItem('locale') || null else null
      locale = stored || $locale.id if !locale?
      service.currentLocale = locale
      promise = $http.get("i18n/#{locale}").then(
        (response) ->
          if $window.localStorage?
            $window.localStorage.setItem 'locale', locale
          mapping = service.mapping = response.data
          promise = null
          service.ready = true
          $locale.id = locale
        ->
          promise = null
          load stored || 'en-us'
      )
  service = (key, params, recurse) ->
    if mapping and key
      deepKey = key.split '.'
      value = mapping
      deepKey.forEach (key) -> value = value[key]
      value = value || "[#{key}]"
      if params?
        params.forEach (param, idx) ->
          param = service(param, null, null) if recurse? and recurse[idx]
          value = value.replace new RegExp("\\{#{idx}\\}", 'g'), param
      value
    else ''
  service.ready = false
  service.mapping = null
  service.load = (locale) -> load locale
  service.locales = [
    'de-de'
    'en-us'
  ]
  service
]