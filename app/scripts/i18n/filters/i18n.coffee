mod = angular.module 'i18n'

mod.filter 'i18n', ['i18n', (i18n) ->
  (messageId, params, recurse) -> i18n messageId, params, recurse
]