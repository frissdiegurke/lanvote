mod = angular.module 'i18n'

mod.directive 'i18n', ['i18n', (i18n) ->
  scope:
    key: '@i18n'
    params: '=i18nValues'
    recurse: '=i18nRecursive'
    html: '=i18nHtml'
  link: ($scope, $element) ->
    $scope.$watchGroup [(() -> i18n.mapping), "key", "params", "recurse"], () ->
      (if $scope.html then $element.html else $element.text).call $element, i18n $scope.key, $scope.params, $scope.recurse
]