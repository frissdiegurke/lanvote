mod = angular.module 'closed'

mod.directive 'progressbar', ['colors', (colors) ->
  restrict: 'EA'
  scope:
    value: "@"
    max: "@"
  transclude: true
  template: '<div ng-transclude></div>'
  link: ($scope, $elem) ->
    value = $scope.value
    $elem.progressbar()
    $indicator = $elem.children '.ui-progressbar-value'
    watch = (attr) ->
      $scope.$watch attr, (value) ->
        $elem.progressbar 'option', attr, Number value
        colors.colorizeElement $indicator, $elem.progressbar('option', 'value'), 0, $elem.progressbar('option', 'max')
    watch 'max'
    watch 'value'
]