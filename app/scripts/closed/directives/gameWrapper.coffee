mod = angular.module 'closed'

mod.directive 'gameWrapper', [ 'voting', '$compile', '$timeout', (voting, $compile, $timeout) ->
  restrict: 'E'
  scope:
    data: "="
    preview: '='
    storageKey: '@'
    button: '&'
  template: '
        <game>
          <img-wrapper ng-click="preview.showImg(data.image)" right-click="preview.showImg(data.icon)"><div bg-img="{{data.icon}}"></div></img-wrapper>
          <div ng-bind="data.title"></div>
          <span class="button" ng-if="data.variants.length" ng-bind="\'form.voting.variants.\' + button.text | i18n" ng-click="button.action()"></span>
          <slider ng-if="data.votingType == 1" value="data.vote" min="0" max="100" step="1" label colorize />
        </game>'
  link: ($scope, $elem) ->
    $scope.button =
      text: 'show'
      classes:
        expanded: false
      action: ->
        $scope.button.classes.expanded = !$scope.button.classes.expanded
        $scope.button.text = if $scope.button.classes.expanded then 'hide' else 'show'
    $scope.button.action() if !$scope.data.votingType
    lastVote = $scope.data.vote
    # TODO nested variants, runtime add/remove of variants
    variants = $scope.data.variants || []
    if variants.length > 0
      $template = $ '<variants ng-class="button.classes"></variants>'
      for vId of variants
        $template.append """<game-wrapper data="data.variants[#{vId}]" preview="preview" ng-if="data.variants[#{vId}].active"></game-wrapper>"""
      $elem.append $compile($template) $scope
    $scope.$watch 'data.vote', (vote) ->
      if vote != lastVote
        parent = voting.grandParentOf $scope.data.id
        parent.classes.unchanged = parent.classes.new = false
      localStorage['votes.' + $scope.data.id] = vote
      voting.ready = false
]