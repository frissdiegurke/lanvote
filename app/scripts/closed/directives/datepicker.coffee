mod = angular.module 'closed'

mod.directive 'datepicker', ['$locale', ($locale) ->
  scope:
    date: "=datepicker"
  restrict: 'A'
  link: ($scope, $elem) ->
    $scope.locale = $locale
    $elem.datepicker
      changeMonth: true
      changeYear: true
      onSelect: ->
        $scope.$apply ->
          $scope.date = $elem.datepicker 'getDate'
    $scope.$watch 'date', (date) ->
      $elem.datepicker 'setDate', date
    $scope.$watch 'locale.id', (id) ->
      $elem.datepicker 'option', $.datepicker.regional[''] if id == 'en-us'
      $elem.datepicker 'option', $.datepicker.regional['de'] if id == 'de-de'
]