mod = angular.module 'closed'

mod.directive 'autocomplete', ->
  scope: {
    model: "="
    select: "&"
    source: "&autocomplete"
    renderItem: "&"
  }
  restrict: 'A'
  link: ($scope, $elem) ->
    $elem.autocomplete {
      minLength: 3
      source: (req, res) ->
        $scope.source req: req, res: res
      select: (e, ui) ->
        $scope.$apply ->
          $scope.select e: e, item: ui.item
    }
    $elem.data( "ui-autocomplete" )._renderItem = (ul, item) ->
      $("<li>").append("<a>#{item.title}</a>").appendTo(ul)