mod = angular.module 'closed'

mod.directive 'editGame', ['games', (games) ->
  restrict: 'E'
  scope:
    game: "="
    save: '&'
    edit: "@"
    preview: "&"
  controller: ['$scope', '$timeout', ($scope, $timeout) ->
    originalGame = $scope.game
    game = $scope.game = angular.copy originalGame
    meta = $scope.meta =
      image:
        source: if game.image then 'serverUrl' else 'url'
        serverUrl: game.image
      icon:
        source: if game.icon then 'serverUrl' else 'url'
        serverUrl: game.icon
    $scope.submit = ->
      angular.copy game, originalGame
      originalGame.image = meta.image[meta.image.source]
      originalGame.icon = meta.icon[meta.icon.source]
      $scope.save()
    $scope.preview = {}
    $scope.imgPreview = (img) ->
      $scope.preview.text = img.width + 'x' + img.height
      $scope.preview.url = img.url
      $scope.preview.show = true
    $scope.gamesSource = (req, res) ->
      games.lookup(name: req.term).then(
        (games) ->
          if games?
            games.forEach (game) ->
              game.label = game.value = game.title
          res(games)
      )
    $scope.gameSelect = (item) ->
      meta.gamesDbResult = item
      game.title = item.title
      game.released = item.released
      game.description = item.description
      game.publisher = item.publisher
      game.genre = item.genre
      game.theGamesDbId = item.theGamesDbId
    $scope.variantSource = (req, res) ->
      res games.all.filter (g) -> g.title.match(new RegExp(req.term))
    $scope.variantSelect = (variant) ->
      $timeout ->
        game.variantOf = variant.id
        meta.variantOf = variant.title
    $scope.chooseGameDbImage = (image) ->
      meta.image.gamesDb = image.url;
      meta.image.source = 'gamesDb'
    $scope.chooseGameDbIcon = (image) ->
      meta.icon.gamesDb = image.url;
      meta.icon.source = 'gamesDb'
    if game.theGamesDbId?
      $scope.loadingFromGameDb = true
      games.lookup(id: game.theGamesDbId).then (games) ->
        meta.gamesDbResult = games[0]
        $scope.loadingFromGameDb = false
  ]
  templateUrl: '/templates/edit-game.html'
]