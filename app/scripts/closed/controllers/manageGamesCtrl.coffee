mod = angular.module 'closed'

mod.controller 'manageGamesCtrl', ['$scope', 'socket', 'toaster', ($scope, socket, toaster) ->
  $scope.newGame =
    votingType: 1
  $scope.createNewGame = ->
    title = $scope.newGame.title
    socket.query('games.create', $scope.newGame).then(
      ->
        toaster.success 'game.added', [title]
      (error) -> toaster.error error
    )['finally'] -> $scope.newGame =
      votingType: 1
]