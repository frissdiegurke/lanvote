mod = angular.module 'closed'

mod.controller 'votingCtrl', ['$scope', 'games', 'voting', ($scope, games, voting) ->
  $scope.games = voting.all = games.all.map (game) -> voting.getGameWithVote(game)
  $scope.preview =
    text: ''
    showImg: (img) ->
      $scope.preview.url = img
      $scope.preview.show = true
]