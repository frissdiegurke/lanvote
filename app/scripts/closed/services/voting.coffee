mod = angular.module 'closed'

mod.factory 'voting', ['socket', '$timeout', '$window', (socket, $timeout, $window) ->
  service =
    all: []
    allowed: false
    ready: false
    peers:
      ready: 0
      total: 0
    range:
      max: 100
      min: 0
      def: 50
    footerClasses:
      ready: false
      'show-chat': false

  title = $window.document.title
  titleCheck = '\u2713'

  setTitle = (ready) ->
    $timeout ->
      $window.document.title = if ready then titleCheck + title else title

  queryState = ->
    socket.query 'status', (data) ->
      $timeout ->
        service.allowed = data.active
        service.ready = data.ready && data.active
        setTitle service.ready

  socket.on 'active', (data) ->
    $timeout ->
      service.allowed = data
      service.ready = false if !data

  socket.on 'connected', queryState

  getVoting = (array, target = {}) ->
    for game in array
      target[game.id] = game.vote if game.votingType != 0
      getVoting game.variants || [], target
    target

  socket.on 'ready', (data) ->
    $timeout ->
      service.peers.ready = data.ready
      service.peers.total = data.total
    if data.ready == data.total and data.total > 0
      socket.emit 'vote', getVoting service.all

  setGameDefaults = (game, isNew) ->
    vote = localStorage['votes.' + game.id] if localStorage?
    game.vote = +vote if vote?
    game.classes =
      new: isNew
    game.vote = service.range.def if !game.vote? || isNaN game.vote
    if game.variants?
      setGameDefaults variant, isNew for variant in game.variants
    game

  service.getGameWithVote = (game, isNew = false) ->
    setGameDefaults angular.copy(game), isNew

  service.addGame = (game) ->
    array = if game.variantOf? then service.byId(game.variantOf)?.variants else service.all
    $timeout ->
      array?.push service.getGameWithVote game, true
      service.ready = false

  loadGame = (array, index, game) ->
    array[index] = service.getGameWithVote game
    service.ready = false

  updateGameVariants = (i, game, newVariant, cursor = game) ->
    for vI of cursor.variants
      v = cursor.variants[vI]
      if v.id == newVariant.id
        loadGame cursor.variants, vI, newVariant
        return true;
      if updateGameVariants i, game, newVariant.id, v # recursive variants
        return true;
    false

  service.updateGame = (newGame) ->
    $timeout ->
      for i of service.all
        game = service.all[i]
        if game.id == newGame.id
          game.classes.unchanged = true
          service.ready = false
          loadGame service.all, i, newGame
          return;
        if updateGameVariants i, game, newGame
          game.classes.unchanged = true
          service.ready = false
          return;

  service.removeGame = (id) ->
    # TODO nested variants
    $timeout ->
      l = service.all.length
      service.all.forEach (g, i) ->
        service.all.splice i, 1 if g.id == id
      if l != service.all.length
        service.ready = false
      service.all.forEach (game, index) ->
        l = game.variants.length
        game.variants.forEach (v, i) ->
          game.variants.splice i, 1 if v.id == id
        if l != game.variants.length
          game.classes.unchanged = true
          service.ready = false
        service.all[index] = angular.copy game

  service.byId = (id, array = service.all) ->
    for game in array
      return game if game.id == id
      if game.variants
        res = service.byId id, game.variants
        return res if res?
    null

  service.grandParentOf = (id, array = service.all) ->
    for game in array
      return game if game.id == id || game.variants && service.grandParentOf(id, game.variants)?
    null

  socket.on 'vote range', (range) ->
    $timeout ->
      service.range = range

  service.load = ->
    socket.query 'vote range', (range) ->
      service.range = range
    queryState()

  service.setReady = (ready) ->
    setTitle ready
    socket.emit 'ready', ready
    if ready
      for game in service.all
        game.classes.unchanged = false
        game.classes.new = false

  service
]