mod = angular.module 'closed'

mod.factory 'games', ['socket', '$q', 'voting', '$timeout', (socket, $q, voting, $timeout) ->
  service = {}
  deferred = null
  service.all = []
  service.load = ->
    if !deferred?
      deferred = $q.defer()
      socket.query 'games', (games) ->
        service.all = games
        deferred.resolve games
    deferred.promise

  addGame = (game) ->
    array = if game.variantOf? then service.getById(game.variantOf).variants else service.all
    $timeout ->
      array.push game
    voting.addGame game

  updateGame = (ref, game) ->
    $timeout ->
      ref[key] = game[key] for key of game
      voting.updateGame ref

  deleteGame = (id) ->
    # TODO nested variants
    $timeout ->
      service.all.forEach (game, index) ->
        if game.id == id
          service.all.splice index, 1
        else
          game.variants.forEach (variant, i) ->
            if variant.id == id
              game.variants.splice i, 1
              service.all[index] = angular.copy service.all[index]
    voting.removeGame id

  addOrUpdate = (games) ->
    games = [games] if !angular.isArray games
    for game in games
      ref = service.getById game.id
      if !ref? then addGame game else updateGame ref, game

  deleteGames = (ids) ->
    ids = [ids] if !angular.isArray ids
    deleteGame id for id in ids

  socket.on 'games.create', addOrUpdate
  socket.on 'games.update', addOrUpdate
  socket.on 'games.delete', deleteGames

  service.create = (game) ->
    socket.query 'games.create', game

  reverseGamePath = (id, target, array) ->
    for game in array
      if game.id == id
        target.push game
        return true
      if game.variants
        if reverseGamePath id, target, game.variants
          target.push game
          return true
    false

  gamePath = (id) ->
    path = []
    reverseGamePath id, path, service.all
    path.reverse()

  service.pathString = (id, separator) ->
    path = ''
    gPath = gamePath id
    path += game.title + (if id == game.id then '' else separator) for game in gPath
    path

  service.getById = (id, array = service.all) ->
    id = Number id
    for game in array
      return game if game.id == id
      if game.variants
        res = service.getById id, game.variants
        return res if res?
    null

  service.lookup = (query) ->
    socket.query 'games.lookup', query
  service
]