mod = angular.module 'closed'

mod.factory 'user', ['socket', '$q', 'toaster', (socket, $q, toaster) ->
  service = {}
  deferred = null
  socket.on 'active', (active) ->
    if active
      toaster.success 'user.activated'
    else
      toaster.error 'user.deactivated'
  service.load = ->
    if !deferred?
      deferred = $q.defer()
      socket.query 'status', (status) ->
        service.status = status
        deferred.resolve status
    deferred.promise
  service
]