mod = angular.module 'util'

mod.directive 'spinner', ['$timeout', ($timeout) ->
  restrice: 'A'
  scope:
    show: '@spinner'
    delay: '@'
  replace: true
  template: '<div class="spinner-container" ng-show="showSpinner"></div>'
  compile: (element, atts) ->

    # set defaults
    atts.delay = if atts.delay? then Number atts.delay else 200

    ($scope, elem) ->

      spinner = null
      timeout = null

      createSpinner = ->
        $scope.showSpinner = true
        timeout = $timeout ->
          size = Math.min elem.width(), elem.height()
          opts =
            lines: 13, # The number of lines to draw
            length: 0.25 * size, # The length of each line
            width: 10 * size / 200, # The line thickness
            radius: 0.125 * size, # The radius of the inner circle
            corners: 1, # Corner roundness (0..1)
            rotate: 0, # The rotation offset
            direction: 1, # 1: clockwise, -1: counterclockwise
            color: '#000', # #rgb or #rrggbb or array of colors
            speed: 1, # Rounds per second
            trail: 60, # Afterglow percentage
            shadow: false, # Whether to render a shadow
            hwaccel: true, # Whether to use hardware acceleration
            className: 'spinner', # The CSS class to assign to the spinner
            zIndex: 2e9, # The z-index (defaults to 2000000000)
            top: 'auto', # Top position relative to parent in px
            left: 'auto' # Left position relative to parent in px
          spinner = new Spinner(opts).spin elem[0]
        , $scope.delay

      destroySpinner = ->
        if timeout?
          if spinner?
            spinner.stop()
          else
            # the timeout has not been resolved yet
            $timeout.cancel timeout
        $scope.showSpinner = false

      $scope.$watch 'show', (show) ->
        if show == 'true'
          createSpinner()
        else
          destroySpinner()
]