mod = angular.module 'util'

mod.directive 'rightClick', ["$parse", ($parse) ->
  restrict: 'A'
  link: ($scope, $element, $attrs) ->
    cb = $parse $attrs.rightClick
    $element.bind 'contextmenu', (event) ->
      $scope.$apply ->
        event.preventDefault()
        cb $scope,
          $event: event
]