mod = angular.module 'util'

mod.factory 'multiDefer', ['$q', ($q) ->
  (promises) ->
    finished = 0
    deferred = $q.defer()
    success = ->
      finished++
      deferred.resolve finished if finished == promises.length
    promises.forEach (promise) ->
      error = (err) ->
        deferred.reject err
      promise.then success, error
    deferred.promise
]