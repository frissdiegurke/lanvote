# docs: https://github.com/mapbox/node-sqlite3/wiki

_ = require 'underscore'
sqlite = require('sqlite3').verbose()

db = new sqlite.Database 'data.sqlite'

_.extend exports,
  db: db