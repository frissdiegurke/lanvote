log = require('./logger') 'event'

events = require 'events'

eb = module.exports = new events.EventEmitter()

if log.hasLevel 'debug'
  emit = eb.emit

  eb.emit = (eventName) ->
    log.debug "emitted: '#{eventName}'"
    emit.apply eb, arguments