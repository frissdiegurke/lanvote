crypto = require 'crypto'
_ = require 'underscore'

encrypt = (phrase) ->
  hash = crypto.createHash 'sha256'
  hash.update phrase
  hash.digest 'hex'

generateSalt = ->
  encrypt new Date().toString()

_.extend exports,
  encrypt: encrypt
  generateSalt: generateSalt