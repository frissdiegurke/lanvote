_ = require 'underscore'

votes = {}
ready = {}

readyCounts = (connections) ->
  openConns = _.filter connections, (conn) -> conn.user.active
  ready: openConns.filter((conn) -> ready[conn.user.username]).length
  total: openConns.length

_.extend exports,
  all: votes
  ready: ready
  readyCounts: readyCounts