fs = require 'fs'
http = require 'http'
md = require 'mkdirp'
log = require('../utils/logger') 'games'
socket = require '../socket'
gamesDB = require '../theGamesDB'
db = require('../sqlite').db # TODO: Move db calls to some model

gamesService = require '../services/games'

md 'dist/game-art'

downloadImage = (options, fileName, cb) ->
  imageData = ''
  http.get options, (res) ->
    res.setEncoding('binary');
    res.on 'data', (chunk) ->
      imageData += chunk;
    res.on 'end', () ->
      fs.writeFile fileName, imageData, 'binary', (err) ->
        return log.warn err if err
        cb()

updateImage = (game, cb) ->
  db.run 'UPDATE games SET image = ? WHERE rowid = ?', game.image, game.id, cb

updateIcon = (game, cb) ->
  db.run 'UPDATE games SET icon = ? WHERE rowid = ?', game.icon, game.id, cb

downloadImagesIfAbsoluteUrl = (game, cb) ->
  image = game.image
  icon = game.icon
  if image and image.indexOf('http://') == 0
    delete game.image
    delete game.icon
    splitted = image.split '.'
    ext = splitted[splitted.length - 1]
    downloadImage image, "dist/game-art/image-#{game.id}.#{ext}", ->
      game.image = "game-art/image-#{game.id}.#{ext}"
      updateImage game, ->
        if icon and icon.indexOf('http://') == 0
          splitted = icon.split '.'
          ext = splitted[splitted.length - 1]
          downloadImage icon, "dist/game-art/icon-#{game.id}.#{ext}", ->
            game.icon = "game-art/icon-#{game.id}.#{ext}"
            updateIcon game, cb
  else
    updateImage game, ->
      updateIcon game, cb

create = (game, cb) ->
  db.serialize ->
    db.run 'INSERT INTO games(theGamesDbId,title,released,publisher,genre,image,icon,description,active,votingType,variantOf) VALUES (?,?,?,?,?,?,?,?,?,?,?)',
      game.theGamesDbId, game.title, game.released, game.publisher, game.genre, '', '', game.description, false, game.votingType, game.variantOf,
    db.all 'SELECT rowid FROM games', (err, ids) ->
      game.id = Math.max.apply null, ids.map((id) -> id.rowid)
      downloadImagesIfAbsoluteUrl game, cb

get = (id, cb) ->
  db.get 'SELECT rowid AS id, * FROM games WHERE rowid = ?', id, cb

update = (game, cb) ->
  db.run 'UPDATE games SET theGamesDbId = ?, title = ?, released = ?, publisher = ?, genre = ?, description = ?, active = ?, votingType = ?, variantOf = ? WHERE rowid = ?',
    game.theGamesDbId, game.title, game.released, game.publisher, game.genre, game.description, game.active, game.votingType, game.variantOf, game.id,
    ->
      downloadImagesIfAbsoluteUrl game, cb

remove = (id, cb) ->
  db.run 'DELETE FROM games WHERE rowid = ?', id, cb

db.serialize () ->
  db.run "CREATE TABLE IF NOT EXISTS games (
      theGamesDbId INT,
      title TEXT,
      released TEXT,
      publisher TEXT,
      genre TEXT,
      image TEXT,
      icon TEXT,
      description TEXT,
      active BOOLEAN,
      votingType TINYINT,
      variantOf INT
    )"

  successOrError = (successCb, errorCb) ->
    (err, data) ->
      if err then errorCb err else successCb data

# register socket interactions

  socket.addQueryRoute 'games', (success, error) ->
    gamesService.all successOrError(success, error)
  socket.addQueryRoute 'games.get', (game, success, error) ->
    get game.id, successOrError(success, error)
  socket.addQueryRoute 'games.create', (game, success, error, user) ->
    create game, successOrError(->
        socket.broadcast 'game.activate', game.title if !user.admin
        socket.broadcast 'games.create', game
      , error)
  socket.addQueryRouteAdmin 'games.update', (game, success, error) ->
    update game, successOrError(->
        socket.broadcast 'games.update', game
      , error)
  socket.addQueryRouteAdmin 'games.delete', (game, success, error) ->
    remove game.id, successOrError( ->
        socket.broadcast 'games.delete', game.id
      , error)
  socket.addQueryRoute 'games.lookup', (game, success, error) ->
    gamesDB.query game, success, error