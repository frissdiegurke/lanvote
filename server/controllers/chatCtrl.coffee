_ = require 'underscore'
eb = require '../utils/eventBus'
log = require('../utils/logger') 'chat'
socket = require '../socket'

conns = require '../services/connections'

getConnections = ->
  _.map conns.all, (conn) ->
    username: conn.user.username
    name: conn.user.username

self = exports

_.extend self,
  handleMessage: (entry, user) ->
    log.debug
    entry.sender =
      username: user.username
      name: user.username
    socket.emit recipient, 'chat.message', entry for recipient in entry.recipients # send to recipients
    socket.emit user.username, 'chat.message.sent', entry # back to sender

  getUsername: (success, error, user) ->
    success user.username

  broadcastConnections: ->
    socket.broadcast 'chat.peers', getConnections()

eb.on 'connection.removed', self.broadcastConnections
eb.on 'socket.authorized', self.broadcastConnections

socket.addListener 'chat.message', self.handleMessage
socket.addQueryRoute 'self.username', self.getUsername