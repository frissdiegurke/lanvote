_ = require 'underscore'
eb = require '../utils/eventBus'
log = require('../utils/logger') 'connectionManager'
socket = require '../socket'

conns = require '../services/connections'

pendingTokens = {}

getStatuses = ->
  _.map conns.all, (conn, username) ->
    username: username
    active: conn.user.active

socket.addQueryRouteAdmin 'connected', (success) -> success getStatuses()

eb.on 'socket.connect', (sock) ->
  sock.on 'authorize', (token) ->
    user = pendingTokens[token]
    if user?
      log.info "The socket '#{sock.id}' is authorized as '#{user.username}'."
      conn = conns.add user, sock
      sock.emit 'authorized'
      eb.emit 'socket.authorized', sock, conn
      delete pendingTokens[token]
      if !user.active
        conns.broadcastToAdmins 'not active', user.username
      conns.broadcastToAdmins 'connected', getStatuses()

eb.on 'user.registered', (user) ->
  conns.broadcastToAdmins 'user.registered', user.username

removeConnection = (socket) ->
  conn = conns.remove socket
  if conn? and !conns.all[conn.user.username]?
    eb.emit 'connection.removed'

eb.on 'socket.disconnect', removeConnection

addToken = (token, user) ->
  pendingTokens[token] = user
  setTimeout (-> delete pendingTokens[token]), 5000

_.extend exports,
  addToken: addToken