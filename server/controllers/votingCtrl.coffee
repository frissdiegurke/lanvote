_ = require 'underscore'
eb = require '../utils/eventBus'
log = require('../utils/logger') 'voting'
socket = require '../socket'

conns = require '../services/connections'
votesState = require '../services/votes'
gamesService = require '../services/games'

voted = {}
ready = votesState.ready
votes = votesState.all

range =
  max: 100
  min:-100
  def: 0

getOwnStatus = (success, error, user) ->
  success {
    username: user.username
    ready: ready[user.username]
    active: conns.all[user.username].user.active
  }

readyCounts = (success) ->
  success votesState.readyCounts conns.all

isReady = (success, error, user) ->
  success ready[user.username]

getRange = (success) ->
  success range

setReady = (rdy, user) ->
  log.info "the user '#{user.username}' changed the ready state to '#{rdy}'"
  ready[user.username] = rdy
  socket.broadcast 'ready', votesState.readyCounts conns.all

getGamesByVote = (vote) ->
  games = []
  for g of votes
    games.push votes[g] if votes[g].vote == vote
  games

markWinner = ->
  maxVote = null
  winners = []
  for gameId of votes
    vote = votes[gameId].vote
    maxVote = vote if !maxVote?
    if vote == maxVote
      winners.push gameId
    else if vote > maxVote
      maxVote = vote
      winners = [gameId]
  winner = winners[Math.floor(Math.random() * winners.length)]
  votes[winner].winner = true if winner?

getActiveVariants = (game) ->
  (variant for variant in game.variants || [] when variant.active)

handleGamesVote = (vote, addend, array) ->
  for game in array
    continue if !game.active
    activeVariants = getActiveVariants game
    if activeVariants.length
      if game.votingType and vote[game.id]?
        handleGamesVote vote, addend + vote[game.id], activeVariants
        continue
      else
        handleGamesVote vote, addend, activeVariants
    if game.votingType and vote[game.id]?
      votes[game.id] = { vote: 0, winner: false } if !votes[game.id]?
      vote[game.id] += addend if vote[game.id] != range.min
      votes[game.id].vote += if vote[game.id] < range.min then range.min else
        if vote[game.id] > range.max then range.max else vote[game.id]

handleVote = (vote, user) ->
  rc = votesState.readyCounts conns.all
  if !voted[user.username] && user.active && rc.total == rc.ready && rc.total > 0
    gamesService.all (err, games) ->
      if !err?
        handleGamesVote vote, 0, games
        voted[user.username] = true
        voteCount = _.filter(conns.all, (conn) -> voted[conn.user.username] && conn.user.active).length
        if voteCount == rc.total
          markWinner()
          socket.broadcast 'result', votes
          voted = {}
          for key of votes
            delete votes[key]

broadcastReadyCounts = ->
  socket.broadcast 'ready', votesState.readyCounts conns.all

addActiveUser = (data) ->
  userConn = conns.all[data.username]
  if userConn?
    userConn.user.active = data.active
    socket.emit data.username, 'active', data.active
    broadcastReadyCounts()

eb.on 'socket.disconnect', broadcastReadyCounts
eb.on 'socket.authorized', broadcastReadyCounts

socket.addQueryRoute 'status', getOwnStatus
socket.addQueryRoute 'ready', readyCounts
socket.addQueryRoute 'ready state', isReady
socket.addQueryRoute 'vote range', getRange
socket.addListener 'ready', setReady
socket.addListener 'vote', handleVote
socket.addListenerAdmin 'user.activate', addActiveUser