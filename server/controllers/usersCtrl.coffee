_ = require 'underscore'
db = require('../sqlite').db
socket = require '../socket'
sec = require '../utils/security'
passport = require 'passport' # TODO: move passport somewhere else
LocalStrategy = require('passport-local').Strategy

encryptPassword = (clearTextPassword, salt) ->
  sec.encrypt "#{clearTextPassword} --- #{salt}"

passwordsMatch = (candidatePassword, originalPassword, salt) ->
  originalPassword == encryptPassword candidatePassword, salt

# setup passport

passport.serializeUser (user, done) ->
  done null, user.id

passport.deserializeUser (id, done) ->
  get id, (err, user) ->
    done err, user

passport.use new LocalStrategy (username, password, done) ->
  getByUsername username, (err, user) ->
    if err
      done err
    else if !user
      done null, false, { message: 'validation.login.invalid' }
    else if passwordsMatch password, user.password, user.salt
      done null, user
    else
      done null, false, { message: 'validation.login.invalid' }

# schema

db.serialize () ->
  db.run "CREATE TABLE IF NOT EXISTS users (
      username TEXT UNIQUE,
      password TEXT,
      salt TEXT,
      admin BOOLEAN,
      active BOOLEAN
    )"
  db.all 'SELECT username FROM users WHERE admin = 1', (error, admins) ->
    if admins.length == 0
      create {
        username: 'admin'
        password: 'password'
        active: true
        admin: true
      }

# db-access

all = (cb) ->
  db.all 'SELECT rowid AS id, username, active FROM users', (error, result) ->
    if !error?
      _.each result, (user) ->
        user.active = user.active == 1
    cb error, result

get = (id, cb) ->
  db.get 'SELECT rowid AS id, * FROM users WHERE rowid = ?', id, cb

create = (user, cb) ->
  user.salt = sec.generateSalt()
  user.password = encryptPassword user.password, user.salt
  db.run 'INSERT INTO users VALUES (?,?,?,?,?)',
    user.username, user.password, user.salt, user.active, user.admin, cb

getByUsername = (username, cb) ->
  db.get 'SELECT rowid AS id, * FROM users WHERE username = ?', username, cb

register = (user, cb) ->
  create(
    username: user.username
    password: user.password
    active: false
    admin: false
  , (err, data) ->
    cb err, data
  )

setActive = (username, active, cb) ->
  db.run 'UPDATE users SET active = ? WHERE username = ?', active, username, cb

# wiring to sockets

successOrError = (successCb, errorCb) ->
  (err, data) ->
    if err then errorCb err else successCb data

bindings =
  loadAll: (success, error) -> all successOrError(success, error)
  setActive: (data) -> setActive data.username, data.active

socket.addQueryRouteAdmin 'users', bindings.loadAll
socket.addListenerAdmin 'user.activate', bindings.setActive

_.extend exports,
  register: register
  getByUsername: getByUsername