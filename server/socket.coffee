io = require('socket.io')()
conns = require './services/connections'
log = require('./utils/logger') 'socket'
eb = require './utils/eventBus'
_ = require 'underscore'

queryRoutes = {}
queryRoutesAdmin = {}

listeners = {}
listenersAdmin = {}

setupQuery = (socket, routes, username) ->
  socket.on 'query', (data) ->
    method = routes[data.method]
    if method?
      log.debug "received query for method '#{data.method}' (user: '#{username}')"
      _id = data._id
      delete data._id
      delete data.method
      respond = (res) ->
        res._id = _id
        socket.emit 'query', res
      success = (data) -> respond data: data
      error = (data) -> respond error: data
      if Object.keys(data).length > 0
        method data, success, error, conns.all[username].user
      else
        method success, error, conns.all[username].user

configUser = (socket, username) ->
  setupQuery socket, queryRoutes, username
  Object.keys(listeners).forEach (messageId) ->
    log.debug "registering listeners for '#{messageId}' (user: '#{username}')"
    socket.on messageId, (data) ->
      log.debug "received '#{messageId}' (user: '#{username}')"
      listeners[messageId].forEach (listener) ->
        listener data, conns.all[username].user

configAdmin = (socket, username) ->
  setupQuery socket, queryRoutesAdmin, username
  Object.keys(listenersAdmin).forEach (messageId) ->
    log.debug "registering listeners for '#{messageId}' (user: '#{username}', admin)"
    socket.on messageId, (data) ->
      log.debug "received '#{messageId}' (user: '#{username}', admin)"
      listenersAdmin[messageId].forEach (listener) ->
        listener data, conns.all[username].user

addListener = (messageId, callback) ->
  log.debug "adding listener to '#{messageId}'"
  listeners[messageId] = listeners[messageId] || []
  listeners[messageId].push callback

addListenerAdmin = (messageId, callback) ->
  log.debug "adding listener to '#{messageId}' (admin)"
  listenersAdmin[messageId] = listenersAdmin[messageId] || []
  listenersAdmin[messageId].push callback

addQueryRoute = (path, callback) ->
  queryRoutes[path] = callback

addQueryRouteAdmin = (path, callback) ->
  queryRoutesAdmin[path] = callback

init = (server) ->
  # config: https://github.com/LearnBoost/Socket.IO/wiki/Configuring-Socket.IO
  io.serveClient(false);
  io.sockets.on 'connection', (socket) ->
    log.debug "'#{socket.id}' connected."
    eb.emit 'socket.connect', socket
    socket.emit 'connection established'
  io = io.attach server

initConnection = (socket) ->
  socket.on 'disconnect', ->
    log.debug "'#{socket.id}' disconnected"
    eb.emit 'socket.disconnect', socket

emit = (username, messageId, data) ->
  log.debug "emitting '#{messageId}' to user '#{username}'"
  conns.all[username]?.emit messageId, data

broadcast = (messageId, data) ->
  io.sockets.emit messageId, data

addListenerToAll = (messageId, callback) ->
  io.sockets.on messageId, callback

# Applies the (query-)listeners, which are registered via addListener(Admin)/addQueryRoute(Admin).
# Should be called after all other connection callbacks are registered!
eb.on 'socket.authorized', (socket, conn) ->
  configUser socket, conn.user.username
  configAdmin socket, conn.user.username if conn.user.admin

eb.on 'server.created', init
eb.on 'socket.connect', initConnection

_.extend exports,
  init: init
  emit: emit
  on: addListenerToAll
  broadcast: broadcast
  addListener: addListener
  addListenerAdmin: addListenerAdmin
  addQueryRoute: addQueryRoute
  addQueryRouteAdmin: addQueryRouteAdmin