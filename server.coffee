_ = require 'underscore'
dev = _.contains process.argv, '--dev'
environment = if dev then 'dev' else 'dist'
protocol = if dev then 'http' else 'https'

conf = require './server/config'

# configure logger and create own logger

require('./server/utils/logger') conf.logLevel[environment]
log = require('./server/utils/logger') 'server'
log.info "Logger Level is set to '#{log.level}'"

eb = require './server/utils/eventBus'

# Be careful when changing this file. The order of the calls are very important!

port = conf.port[environment]

socket = require './server/socket'
require('./server/serverSetup').createServer(protocol).listen port

# instantiate controllers

require './server/controllers/usersCtrl'
require './server/controllers/chatCtrl'
require './server/controllers/connectionCtrl'
require './server/controllers/gamesCtrl'
require './server/controllers/votingCtrl'

log.info "The server is listening on #{protocol}://localhost:#{port}"